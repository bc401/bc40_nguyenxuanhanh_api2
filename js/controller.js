function batLoading() {
    document.getElementById("spinner").style.display = "flex";

}

function tatLoading() {
    document.getElementById("spinner").style.display = "none";
}

function layThongTinTuForm() {
    const maMon = document.getElementById("maMon").value;
    const tenMon = document.getElementById("tenMon").value;
    const giaMon = document.getElementById("giaMon").value;
    const hinhAnh = document.getElementById("hinhAnh").value;
    const loaiMon = document.getElementById("loaiMon").value;

    var monAn = {
        maMon,
        tenMon,
        giaMon,
        hinhAnh,
        loaiMon,
    };
    return monAn;
}

function convertString(maxlength, value) {
    if(value.length > maxlength) {
        return value.slice(0, maxlength) + "...";

    } else {
        return value;
    }
}