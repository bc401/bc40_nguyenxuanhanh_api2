const BASE_URL = "https://63b2c99e5901da0ab36dbac7.mockapi.io";
function renderFoodList(foods) {
    // true => mặn
    // false => chay
    var contentHTML = "";

    foods.reverse().forEach(function(item) {
        var contentTr = `<tr>
        <td>${item.maMon}</td>
        <td>${item.tenMon}</td>
        <td>${item.giaMon}</td>
        <td>${item.loaiMon ? "<span class='text-primary'>Mặn</span>" 
                            : "<span class='text-success'>Chay</span>"}</td>
        <td>${convertString(50, item.hinhAnh)}</td>
        <td> 
        <button onclick="xoaMonAn('${item.maMon}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaMonAn('${item.maMon}')" class="btn btn-warning">Sửa</button>
        </td>
        </tr>`;
        contentHTML += contentTr;
    })
    document.getElementById("tbodyFood").innerHTML = contentHTML;
}

// Gọi API lấy danh sách món ăn từ server
function fetchFoodList() {
    batLoading();
axios({
    url: `${BASE_URL}/food`,
    method: "GET",
}) 
    .then(function(res) {
        tatLoading();
    var foodList = res.data;
    renderFoodList(foodList);

    })
    .catch(function(err) {
    tatLoading();
    console.log("🚀 ~ file: main.js:38 ~ fetchFoodList ~ err", err)
    });
}
fetchFoodList();
// Xóa mớn ăn

function xoaMonAn(id) {
    batLoading();
    axios( {
        url: `${BASE_URL}/food/${id}`,
        method: "DELETE",

    })
    .then(function(res) {
        tatLoading();
        // gọi lại api lấy danh sách khi xóa thành công
    console.log("🚀 ~ file: main.js:31 ~ .then ~ res", res);
    fetchFoodList();

    })
    .catch(function(err) {
        tatLoading();
        console.log("🚀 ~ file: main.js:35 ~ xoaMonAn ~ err", err);
        fetchFoodList();
    })
}

function themMonAn() {
    var monAn = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/food`,
        method: "POST",
        data: monAn,
    }).then(function(res) {
        fetchFoodList();
    }).catch(function(err) {
        console.log("🚀 ~ file: main.js:76 ~ themMonAn ~ err", err);  
    })
}

 
function suaMonAn(id) {
    batLoading();
    document.getElementById('maMon').disabled = true;
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "GET"
    }).then(function(res) {
        tatLoading();
        console.log("🚀 ~ file: main.js:85 ~ suaMonAn ~ res", res)
        
    }).catch(function(err){
        tatLoading();
        console.log("🚀 ~ file: main.js:87 ~ suaMonAn ~ err", err)
    })
}

function capNhatMonAn() {
 var monAn = layThongTinTuForm();
 axios({
    url: `${BASE_URL}/food/${monAn.maMon}`,
    method: "PUT",
    data: monAn,
 });
}
// pending, resolve (success), reject (fail)
// promise all, promise chaining